#include "model.h"
#include <myHeaders/integrator.h>
#include <ctime>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <boost/format.hpp>
using boost::format;

int main(int ac, char * av[])
{
  try
  {
    //create directories for storing log files
    mkdir("logs", S_IRWXU | S_IRWXG | S_IRWXO);
    mkdir("logs/trace-sigma", S_IRWXU | S_IRWXG | S_IRWXO);

    string config_file;

    double * params = new double[3];
    int times;
    bool del_old_file_flag = true;

    string init;
    int iters_per_step;
    double noise_per_step;
    int save_step;
    int display_step;
    double sigma_end, sigma_steps;

    //
    //=========== start parsing arguments ===========
    //
    // Declare a group of options that will be
    // allowed only on command line
    po::options_description generic("Generic options");
    generic.add_options()
      ("help,h",
        "produce help message")
      ("config,c",
        po::value<string>(&config_file)->default_value("parameters.cfg"),
        "name of a file of a configuration.")
    ;

    // Declare a group of options that will be
    // allowed both on command line and in
    // config file
    po::options_description config("Configuration");
    config.add_options()
      ("start",
        po::value<double>(params+2),
        "start value of coupling strength for tracing solution")
      ("end",
        po::value<double>(&sigma_end),
        "end value of coupling strength for tracing solultion")
      ("steps",
        po::value<double>(&sigma_steps),
        "step of coupling strength for tracing solultion")
      ("coupled,p",
        po::value<double>(params+1),
        "number of coupled oscillators")
      ("times,t",
        po::value<int>(&times),
        "number of integration steps")
      ("nodel",
        "if checked, appends new log info to an existing .log file")
    ;

    // Hidden options, will be allowed both on command line and
    // in config file, but will not be shown to the user.
    po::options_description hidden("Hidden options");
    hidden.add_options()
      ("init",
        po::value<string>(&init),
        "initial conditions")
      ("oscillators,n",
        po::value<double>(params),
        "set number of oscillators in ansamble")
      ("iters-per-step",
        po::value<int>(&iters_per_step),
        "redefine number of iterations per step")
      ("noise-per-step",
        po::value<double>(&noise_per_step),
        "order of random noise added after each step")
      ("save-step",
        po::value<int>(&save_step),
        "frequency of saving data to a .log file")
      ("display-step",
        po::value<int>(&display_step),
        "frequency of reporting progress to terminal")
    ;

    po::options_description cmdline_options;
    cmdline_options.add(generic).add(config).add(hidden);

    po::options_description config_file_options;
    config_file_options.add(config).add(hidden);

    po::options_description visible("Allowed options");
    visible.add(generic).add(config);

    po::positional_options_description p;
    p.add("init", -1);

    po::variables_map vm;
    store(po::command_line_parser(ac, av).
          options(cmdline_options).positional(p).run(), vm);
    notify(vm);

    ifstream ifs(config_file.c_str());
    if (!ifs)
    {
      cout << "can not open config file: " << config_file << "\n";
      return 0;
    }
    else
    {
      store(parse_config_file(ifs, config_file_options), vm);
      notify(vm);
    }

    if (vm.count("help"))
    {
      cout << visible << "\n";
      return 0;
    }
    if (vm.count("nodel"))
    {
      del_old_file_flag = false;
    }

    //=========== end parsing ===========

    int len_x = NVARS * params[0];
    double * x = new double[len_x];
    format log_format("logs/trace-sigma/s%1$.15g_p%2$.5g.dat");

    while ((sigma_steps > 0) ?
                      (params[2] <= sigma_end):(params[2] >= sigma_end))
    {
      load_vect_from_log(x, len_x, init);
      log_format % params[2] % params[1];
      if (del_old_file_flag)
      {
          remove(log_format.str().c_str());
      }
      if (integrate(x, len_x, Model, params, times, log_format.str(),
               iters_per_step, noise_per_step, save_step, display_step))
      {
          init = log_format.str();
          params[2] += sigma_steps;
      }
      else
      {
          cout << "Integration stopped! \n    (no solution...)\n";
          break;
      }
    }

    delete[] params;
    delete[] x;
  }
  catch(exception& e)
  {
    cout << e.what() << endl;
    return 1;
  }
  return 0;
}
