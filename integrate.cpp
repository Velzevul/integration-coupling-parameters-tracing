#include "model.h"
#include <myHeaders/integrator.h>
#include <ctime>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

int main(int ac, char* av[])
{
  try
  {
    //create directory for saving log files
    mkdir("logs", S_IRWXU | S_IRWXG | S_IRWXO);

    string config_file;

    double * params = new double[3];
    int times;
    string log_file;
    bool del_old_file_flag = true;

    string init;
    int iters_per_step;
    double noise_per_step;
    int save_step;
    int display_step;
    //
    //=========== start parsing arguments ===========
    //
    // Declare a group of options that will be
    // allowed only on command line
    po::options_description generic("Generic options");
    generic.add_options()
      ("help,h",
        "produce help message")
      ("config,c",
        po::value<string>(&config_file)->default_value("parameters.cfg"),
        "name of a file of a configuration.")
    ;

    // Declare a group of options that will be
    // allowed both on command line and in
    // config file
    po::options_description config("Configuration");
    config.add_options()
      ("sigma,s",
        po::value<double>(params+2),
        "value of coupling strength parameter")
      ("coupled,p",
        po::value<double>(params+1),
        "number of coupled oscillators")
      ("times,t",
        po::value<int>(&times),
        "number of integration steps")
      ("alias",
        po::value<string>(&log_file),
        "name of the resulting .log file")
      ("nodel",
        "if checked, appends new log info to an existing .log file")
    ;

    // Hidden options, will be allowed both on command line and
    // in config file, but will not be shown to the user.
    po::options_description hidden("Hidden options");
    hidden.add_options()
      ("init",
        po::value<string>(&init),
        "initial conditions")
      ("oscillators,n",
        po::value<double>(params),
        "set number of oscillators in ansamble")
      ("iters-per-step",
        po::value<int>(&iters_per_step),
        "redefine number of iterations per step")
      ("noise-per-step",
        po::value<double>(&noise_per_step),
        "order of random noise added after each step")
      ("save-step",
        po::value<int>(&save_step),
        "frequency of saving data to a .log file")
      ("display-step",
        po::value<int>(&display_step),
        "frequency of reporting progress to terminal")
    ;

    po::options_description cmdline_options;
    cmdline_options.add(generic).add(config).add(hidden);

    po::options_description config_file_options;
    config_file_options.add(config).add(hidden);

    po::options_description visible("Allowed options");
    visible.add(generic).add(config);

    po::positional_options_description p;
    p.add("init", -1);

    po::variables_map vm;
    store(po::command_line_parser(ac, av).
          options(cmdline_options).positional(p).run(), vm);
    notify(vm);

    ifstream ifs(config_file.c_str());
    if (!ifs)
    {
      cout << "can not open config file: " << config_file << "\n";
      return 0;
    }
    else
    {
      store(parse_config_file(ifs, config_file_options), vm);
      notify(vm);
    }

    if (vm.count("help"))
    {
      cout << visible << "\n";
      return 0;
    }
    if (vm.count("nodel"))
    {
      del_old_file_flag = false;
    }

    //=========== end parsing ===========

    log_file = "logs/" + log_file + ".dat";

    if (del_old_file_flag)
    {
        remove(log_file.c_str());
    }

    int len_x = NVARS * params[0];
    double * x = new double[len_x];
    load_vect_from_log(x, len_x, init);

    clock_t start = clock();
    if (integrate(x, len_x, Model, params, times, log_file,
              iters_per_step, noise_per_step, save_step, display_step))
    {
         cout << "Integration finished\n";
    }
    else
    {
        cout << "Integration stopped! \n    (no solution...)\n";
    }
    clock_t ends = clock();
    cout << "-------------------------\n"
         << "integration time: "
         << (double) (ends - start) / CLOCKS_PER_SEC << endl
         << "log saved to: " << log_file << endl;
    delete[] x;
    delete[] params;
  }
  catch(exception& e)
  {
    cout << e.what() << endl;
    return 1;
  }
  return 0;
}
