#ifndef __MODEL_SIN_H__
#define __MODEL_SIN_H__

#define MODEL_NAME "test sinus"
#define NVARS 2

#include <math.h>

void Model(double * X, double * params, double * res)
{
  int n = params[0];
  for (int i=0; i<n; i++)
  {
    res[i] = sin(X[i]) + params[2];
    res[i+n] = cos(X[i+n]);
  }
}

#endif

